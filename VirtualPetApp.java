import java.util.Scanner;

public class VirtualPetApp{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		
		// 1. pack (group of dog)
		Dog[] pack = new Dog[4];

		for(int i = 0; i < pack.length; i++){
			pack[i] = new Dog();
			System.out.println("Input weight");
			pack[i].weight = Double.parseDouble(scan.nextLine());
			System.out.println("Input breed");
			pack[i].breed = scan.nextLine();
			System.out.println("Input color");
			pack[i].color = scan.nextLine();
			
			
		}
		System.out.println("The last weight : " + pack[pack.length-1].weight);
		System.out.println("The last breed : " + pack[pack.length-1].breed);
		System.out.println("The last color : " + pack[pack.length-1].color);
		
		pack[0].goEat();
		pack[0].goCrazy();
	}
}
